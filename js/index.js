import * as rest from "./rest.js"

function showCar(id) {
  rest.getCar(id).then(car => document.getElementById("car").textContent = JSON.stringify(car))
}

function showCars() {
  rest.getCars().then(cars => document.getElementById("cars").textContent = JSON.stringify(cars))
}

function showCarOwner(id) {
  rest.getCarOwner(id).then(person => document.getElementById("owner-of-car").textContent = JSON.stringify(person))
}

function showCarAndOwner(id) {
  rest.getCarAndOwner(id).then(result =>
    document.getElementById("owner-and-car").innerHTML =
      `<div> car = ${JSON.stringify(result.car)} </div>
       <div> owner=${JSON.stringify(result.person)}</div>`);
}

document.getElementById("carId").addEventListener("blur",e => {
  const id = e.target.value
  showCars();
  showCar(id);
  showCarOwner(id);
  showCarAndOwner(id);
})
